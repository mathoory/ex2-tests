#!/usr/bin/env bash

NAME=submission_test.zip
FINAL=ex2.zip

pushd src
    mv part{_a,A}
    mv part{_b,B}
    zip "../${NAME}" part{A,B}/*.{h,cpp}
    mv part{A,_a}
    mv part{B,_b}
popd

zip "${NAME}" -d partA/part_a_test.cpp partB/{Auxiliaries,gameTest}.{h,cpp}

cmake --build build --target dry-pdf
zip -j "${NAME}" build/dry.pdf

./finalcheck "${NAME}"

rm -i "${NAME}"
if [ -f "${NAME}" ]; then
    echo "Saving ${NAME} as ${FINAL}"
    mv "${NAME}" "${FINAL}"
fi
